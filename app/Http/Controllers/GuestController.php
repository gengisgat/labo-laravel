<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GuestController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function listproducts($categoryID = '%')
    {
//    dd($categoryID);
    $category = \App\ProductCategory::where('id', $categoryID)->get();
    $products = \App\Product::select('products.id', 'products.name', 'products.description', 'products.price', 'file_name', 'media.id as media_id')
        ->join('product_product_category', 'product_product_category.product_id', '=', 'products.id')
        ->join('media', 'media.model_id', '=', 'products.id')
        ->where('media.model_type', 'App\\product')
        ->where('product_product_category.product_category_id', 'like', $categoryID)
        ->get();
//    dd($products);

    return view('product.index', compact('category', 'products'));
    }
}

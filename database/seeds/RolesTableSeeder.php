<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('roles')->delete();
        
        \DB::table('roles')->insert(array (
            0 => 
            array (
                'id' => 1,
                'title' => 'Admin',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'title' => 'Gestore prodotti',
                'created_at' => NULL,
                'updated_at' => '2020-04-29 15:16:27',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'title' => 'User semplice',
                'created_at' => '2020-04-29 15:16:10',
                'updated_at' => '2020-04-29 15:16:10',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}
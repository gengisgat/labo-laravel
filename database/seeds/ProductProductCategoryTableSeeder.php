<?php

use Illuminate\Database\Seeder;

class ProductProductCategoryTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('product_product_category')->delete();
        
        \DB::table('product_product_category')->insert(array (
            0 => 
            array (
                'product_id' => 2,
                'product_category_id' => 5,
            ),
            1 => 
            array (
                'product_id' => 1,
                'product_category_id' => 3,
            ),
            2 => 
            array (
                'product_id' => 3,
                'product_category_id' => 4,
            ),
            3 => 
            array (
                'product_id' => 4,
                'product_category_id' => 2,
            ),
            4 => 
            array (
                'product_id' => 5,
                'product_category_id' => 2,
            ),
            5 => 
            array (
                'product_id' => 6,
                'product_category_id' => 1,
            ),
            6 => 
            array (
                'product_id' => 7,
                'product_category_id' => 2,
            ),
            7 => 
            array (
                'product_id' => 8,
                'product_category_id' => 3,
            ),
            8 => 
            array (
                'product_id' => 8,
                'product_category_id' => 4,
            ),
            9 => 
            array (
                'product_id' => 9,
                'product_category_id' => 2,
            ),
            10 => 
            array (
                'product_id' => 9,
                'product_category_id' => 5,
            ),
            11 => 
            array (
                'product_id' => 10,
                'product_category_id' => 1,
            ),
            12 => 
            array (
                'product_id' => 11,
                'product_category_id' => 3,
            ),
            13 => 
            array (
                'product_id' => 11,
                'product_category_id' => 5,
            ),
            14 => 
            array (
                'product_id' => 12,
                'product_category_id' => 1,
            ),
            15 => 
            array (
                'product_id' => 12,
                'product_category_id' => 3,
            ),
            16 => 
            array (
                'product_id' => 13,
                'product_category_id' => 1,
            ),
            17 => 
            array (
                'product_id' => 14,
                'product_category_id' => 3,
            ),
        ));
        
        
    }
}
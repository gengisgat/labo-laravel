<?php

use Illuminate\Database\Seeder;

class ProductProductTagTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('product_product_tag')->delete();
        
        \DB::table('product_product_tag')->insert(array (
            0 => 
            array (
                'product_id' => 2,
                'product_tag_id' => 2,
            ),
            1 => 
            array (
                'product_id' => 1,
                'product_tag_id' => 1,
            ),
            2 => 
            array (
                'product_id' => 3,
                'product_tag_id' => 1,
            ),
            3 => 
            array (
                'product_id' => 4,
                'product_tag_id' => 1,
            ),
            4 => 
            array (
                'product_id' => 5,
                'product_tag_id' => 1,
            ),
            5 => 
            array (
                'product_id' => 6,
                'product_tag_id' => 1,
            ),
            6 => 
            array (
                'product_id' => 6,
                'product_tag_id' => 2,
            ),
            7 => 
            array (
                'product_id' => 7,
                'product_tag_id' => 1,
            ),
            8 => 
            array (
                'product_id' => 7,
                'product_tag_id' => 3,
            ),
            9 => 
            array (
                'product_id' => 8,
                'product_tag_id' => 1,
            ),
            10 => 
            array (
                'product_id' => 9,
                'product_tag_id' => 2,
            ),
            11 => 
            array (
                'product_id' => 10,
                'product_tag_id' => 1,
            ),
            12 => 
            array (
                'product_id' => 10,
                'product_tag_id' => 2,
            ),
            13 => 
            array (
                'product_id' => 11,
                'product_tag_id' => 1,
            ),
            14 => 
            array (
                'product_id' => 11,
                'product_tag_id' => 3,
            ),
            15 => 
            array (
                'product_id' => 12,
                'product_tag_id' => 1,
            ),
            16 => 
            array (
                'product_id' => 12,
                'product_tag_id' => 2,
            ),
            17 => 
            array (
                'product_id' => 13,
                'product_tag_id' => 1,
            ),
            18 => 
            array (
                'product_id' => 13,
                'product_tag_id' => 2,
            ),
            19 => 
            array (
                'product_id' => 14,
                'product_tag_id' => 1,
            ),
            20 => 
            array (
                'product_id' => 14,
                'product_tag_id' => 3,
            ),
        ));
        
        
    }
}
<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'admin',
                'email' => 'admin@admin.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$4f3JhKOXbwnuqhng1X2hiuNWI4QooX3MiRMwJ0fMELvc9/RkRCE9G',
                'remember_token' => NULL,
                'firstname' => 'admin',
                'lastname' => 'admin',
                'created_at' => NULL,
                'updated_at' => '2020-04-29 15:17:06',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'lino.topo',
                'email' => 'lino.topo@la.la',
                'email_verified_at' => NULL,
                'password' => '$2y$10$4w1cgV7nPM1IbZeoIo6qhuwpJNJH8ZybuAm.fA5qPjIjyOmJTtc2O',
                'remember_token' => NULL,
                'firstname' => 'Lino',
                'lastname' => 'Topo',
                'created_at' => '2020-04-29 15:15:10',
                'updated_at' => '2020-04-29 15:15:10',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'rino.pape',
                'email' => 'rino.pape@la.la',
                'email_verified_at' => NULL,
                'password' => '$2y$10$U.EsMv6.xspnFs8Fzujqce4M9VVGd8761o6Dnndumh.UjzEOk1V5u',
                'remember_token' => NULL,
                'firstname' => 'Rino',
                'lastname' => 'Pape',
                'created_at' => '2020-04-29 15:18:04',
                'updated_at' => '2020-04-29 15:18:04',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}
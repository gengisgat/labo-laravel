<?php

use Illuminate\Database\Seeder;

class ProductTagsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('product_tags')->delete();
        
        \DB::table('product_tags')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Divertente',
                'created_at' => '2020-04-29 11:26:32',
                'updated_at' => '2020-04-29 11:26:32',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Utile',
                'created_at' => '2020-04-29 11:26:40',
                'updated_at' => '2020-04-29 11:26:40',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Bambini',
                'created_at' => '2020-04-29 11:27:17',
                'updated_at' => '2020-04-29 11:27:17',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}
<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        // Seeding ottenuti con la libreria iseed (https://github.com/orangehill/iseed)
        // il comando effettuato è per esempio php artisan iseed product_categories (un comando per tabella)
        $this->call(MediaTableSeeder::class);
        $this->call(PermissionsTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(PermissionRoleTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(RoleUserTableSeeder::class);
        $this->call(MediaTableSeeder::class);
        $this->call(ProductsTableSeeder::class);
        $this->call(ProductCategoriesTableSeeder::class);
        $this->call(ProductTagsTableSeeder::class);
        $this->call(ProductProductCategoryTableSeeder::class);
        $this->call(ProductProductTagTableSeeder::class);
    }
}

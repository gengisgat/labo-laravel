<?php

use Illuminate\Database\Seeder;

class PermissionRoleTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('permission_role')->delete();
        
        \DB::table('permission_role')->insert(array (
            0 => 
            array (
                'role_id' => 1,
                'permission_id' => 1,
            ),
            1 => 
            array (
                'role_id' => 1,
                'permission_id' => 2,
            ),
            2 => 
            array (
                'role_id' => 1,
                'permission_id' => 3,
            ),
            3 => 
            array (
                'role_id' => 1,
                'permission_id' => 4,
            ),
            4 => 
            array (
                'role_id' => 1,
                'permission_id' => 5,
            ),
            5 => 
            array (
                'role_id' => 1,
                'permission_id' => 6,
            ),
            6 => 
            array (
                'role_id' => 1,
                'permission_id' => 7,
            ),
            7 => 
            array (
                'role_id' => 1,
                'permission_id' => 8,
            ),
            8 => 
            array (
                'role_id' => 1,
                'permission_id' => 9,
            ),
            9 => 
            array (
                'role_id' => 1,
                'permission_id' => 10,
            ),
            10 => 
            array (
                'role_id' => 1,
                'permission_id' => 11,
            ),
            11 => 
            array (
                'role_id' => 1,
                'permission_id' => 12,
            ),
            12 => 
            array (
                'role_id' => 1,
                'permission_id' => 13,
            ),
            13 => 
            array (
                'role_id' => 1,
                'permission_id' => 14,
            ),
            14 => 
            array (
                'role_id' => 1,
                'permission_id' => 15,
            ),
            15 => 
            array (
                'role_id' => 1,
                'permission_id' => 16,
            ),
            16 => 
            array (
                'role_id' => 1,
                'permission_id' => 17,
            ),
            17 => 
            array (
                'role_id' => 1,
                'permission_id' => 18,
            ),
            18 => 
            array (
                'role_id' => 1,
                'permission_id' => 19,
            ),
            19 => 
            array (
                'role_id' => 1,
                'permission_id' => 20,
            ),
            20 => 
            array (
                'role_id' => 1,
                'permission_id' => 21,
            ),
            21 => 
            array (
                'role_id' => 1,
                'permission_id' => 22,
            ),
            22 => 
            array (
                'role_id' => 1,
                'permission_id' => 23,
            ),
            23 => 
            array (
                'role_id' => 1,
                'permission_id' => 24,
            ),
            24 => 
            array (
                'role_id' => 1,
                'permission_id' => 25,
            ),
            25 => 
            array (
                'role_id' => 1,
                'permission_id' => 26,
            ),
            26 => 
            array (
                'role_id' => 1,
                'permission_id' => 27,
            ),
            27 => 
            array (
                'role_id' => 1,
                'permission_id' => 28,
            ),
            28 => 
            array (
                'role_id' => 1,
                'permission_id' => 29,
            ),
            29 => 
            array (
                'role_id' => 1,
                'permission_id' => 30,
            ),
            30 => 
            array (
                'role_id' => 1,
                'permission_id' => 31,
            ),
            31 => 
            array (
                'role_id' => 1,
                'permission_id' => 32,
            ),
            32 => 
            array (
                'role_id' => 1,
                'permission_id' => 33,
            ),
            33 => 
            array (
                'role_id' => 2,
                'permission_id' => 17,
            ),
            34 => 
            array (
                'role_id' => 2,
                'permission_id' => 18,
            ),
            35 => 
            array (
                'role_id' => 2,
                'permission_id' => 19,
            ),
            36 => 
            array (
                'role_id' => 2,
                'permission_id' => 20,
            ),
            37 => 
            array (
                'role_id' => 2,
                'permission_id' => 21,
            ),
            38 => 
            array (
                'role_id' => 2,
                'permission_id' => 22,
            ),
            39 => 
            array (
                'role_id' => 2,
                'permission_id' => 23,
            ),
            40 => 
            array (
                'role_id' => 2,
                'permission_id' => 24,
            ),
            41 => 
            array (
                'role_id' => 2,
                'permission_id' => 25,
            ),
            42 => 
            array (
                'role_id' => 2,
                'permission_id' => 26,
            ),
            43 => 
            array (
                'role_id' => 2,
                'permission_id' => 27,
            ),
            44 => 
            array (
                'role_id' => 2,
                'permission_id' => 28,
            ),
            45 => 
            array (
                'role_id' => 2,
                'permission_id' => 29,
            ),
            46 => 
            array (
                'role_id' => 2,
                'permission_id' => 30,
            ),
            47 => 
            array (
                'role_id' => 2,
                'permission_id' => 31,
            ),
            48 => 
            array (
                'role_id' => 2,
                'permission_id' => 32,
            ),
            49 => 
            array (
                'role_id' => 2,
                'permission_id' => 33,
            ),
            50 => 
            array (
                'role_id' => 3,
                'permission_id' => 17,
            ),
            51 => 
            array (
                'role_id' => 3,
                'permission_id' => 20,
            ),
            52 => 
            array (
                'role_id' => 3,
                'permission_id' => 22,
            ),
            53 => 
            array (
                'role_id' => 3,
                'permission_id' => 25,
            ),
            54 => 
            array (
                'role_id' => 3,
                'permission_id' => 27,
            ),
            55 => 
            array (
                'role_id' => 3,
                'permission_id' => 30,
            ),
            56 => 
            array (
                'role_id' => 3,
                'permission_id' => 32,
            ),
        ));
        
        
    }
}
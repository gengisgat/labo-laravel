<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('products')->delete();
        
        \DB::table('products')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Antistress',
                'description' => 'Mai più stressati',
                'price' => '8.00',
                'created_at' => '2020-04-29 11:29:36',
                'updated_at' => '2020-04-29 11:29:36',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Appendiabiti',
                'description' => 'Per non perdere mai le proprie chiavi',
                'price' => '16.00',
                'created_at' => '2020-04-29 11:31:10',
                'updated_at' => '2020-04-29 11:31:10',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Bagnoschiuma',
                'description' => 'Bombe da bagno dal profumo inebriante',
                'price' => '6.00',
                'created_at' => '2020-04-29 12:22:15',
                'updated_at' => '2020-04-29 14:23:42',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Calze',
                'description' => 'Calze per party alcolici',
                'price' => '11.00',
                'created_at' => '2020-04-29 15:00:02',
                'updated_at' => '2020-04-29 15:00:02',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Costume Sumo',
                'description' => 'A carnevale mai più senza',
                'price' => '31.00',
                'created_at' => '2020-04-29 15:01:00',
                'updated_at' => '2020-04-29 15:01:00',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'Frullatore',
                'description' => 'Tritatutto senza pietà',
                'price' => '34.00',
                'created_at' => '2020-04-29 15:02:35',
                'updated_at' => '2020-04-29 15:02:35',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'Maglietta "Non Mi Serve Google..."',
                'description' => 'Maglietta "non mi Serve Google, mia moglie sa tutto!"',
                'price' => '14.00',
                'created_at' => '2020-04-29 15:03:41',
                'updated_at' => '2020-04-29 15:03:41',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'Materassino',
                'description' => 'Materassino avocado, e non cado più in acqua',
                'price' => '19.00',
                'created_at' => '2020-04-29 15:04:38',
                'updated_at' => '2020-04-29 15:04:38',
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'Piega magliette',
                'description' => 'Basta con i vestiti sgualciti',
                'price' => '19.00',
                'created_at' => '2020-04-29 15:05:25',
                'updated_at' => '2020-04-29 15:05:25',
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'Portaspugna',
                'description' => 'Lavare i piatti sarà più divertente',
                'price' => '5.00',
                'created_at' => '2020-04-29 15:06:20',
                'updated_at' => '2020-04-29 15:06:20',
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'name' => 'Pulsante antipanico',
                'description' => 'Vi sentirà tutto il vicinato',
                'price' => '17.00',
                'created_at' => '2020-04-29 15:07:19',
                'updated_at' => '2020-04-29 15:07:19',
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'name' => 'Sprargizucchero',
                'description' => 'La torta sarà magicamente più buona',
                'price' => '9.00',
                'created_at' => '2020-04-29 15:08:06',
                'updated_at' => '2020-04-29 15:08:06',
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'name' => 'Tagliapizze',
                'description' => 'Ideale per tutti i formati di pizza',
                'price' => '8.00',
                'created_at' => '2020-04-29 15:10:34',
                'updated_at' => '2020-04-29 15:10:34',
                'deleted_at' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'name' => 'Wrestling per pollici',
                'description' => 'Lotta senza esclusione di colpi per grandi e piccini',
                'price' => '12.00',
                'created_at' => '2020-04-29 15:12:44',
                'updated_at' => '2020-04-29 15:12:44',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}
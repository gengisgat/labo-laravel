<?php

use Illuminate\Database\Seeder;

class ProductCategoriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('product_categories')->delete();
        
        \DB::table('product_categories')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Cucina',
                'description' => 'Tutto quello che potete desiderare in cucina',
                'created_at' => '2020-04-29 11:23:05',
                'updated_at' => '2020-04-29 11:23:05',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Vestiti',
                'description' => 'Vestiario di ogni tipo',
                'created_at' => '2020-04-29 11:23:26',
                'updated_at' => '2020-04-29 11:23:26',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Giochi',
                'description' => 'I più divertenti giochi al mondo',
                'created_at' => '2020-04-29 11:26:03',
                'updated_at' => '2020-04-29 11:26:03',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Bagno',
                'description' => 'Cose utili per il bagno',
                'created_at' => '2020-04-29 11:27:51',
                'updated_at' => '2020-04-29 11:28:00',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Casa',
                'description' => 'Tutto quello che può servire in casa',
                'created_at' => '2020-04-29 11:30:43',
                'updated_at' => '2020-04-29 11:30:43',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}
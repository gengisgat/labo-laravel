@extends('layouts.layoutGuest')

@section('title')
    Home shoppete
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12"><h2 class="text-center">Scegli una categoria</h2></div>
    {{--        <div class="col-lg-2 col-md-4 col-sm-6">        </div>--}}
            @foreach($categories as $category)
                <div class="card" style="width: 20rem;">
                    {{--      <img class="card-img-top" src="holder.js/100px80" alt="Card image cap">--}}
                    <div class="card-body">
                        <h4 class="card-title">{{$category->name}}</h4>
                        <p class="card-text">{{$category->description}}</p>
                        <a href="{{url('products') . '/'. $category->id}}" class="btn btn-primary">Vai ai prodotti</a>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection


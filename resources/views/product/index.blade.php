@extends('layouts.layoutGuest')

@section('title')
    Home shoppete
@endsection

@section('content')

    @if($category->count() > 0)
        <h2 class="text-center">{{$category->first()->name}}</h2>
        <p class="text-center">{{$category->first()->description}}</p>
    @else
        <h2 class="text-center">Prodotti</h2>
        <p class="text-center">Tutti i prodotti</p>
    @endif
    <hr>

    <div class="container">
        <div class="row">
            @foreach($products as $product)
                <div class="card" style="width: 20rem;"> {{-- emmet bs4-card --}}

    {{--                <img class="card-img-top" src="{{asset('storage') . '/' . $product->media_id . '/' . $product->file_name}}" alt="Card image cap">--}}
                    <img class="card-img-top" src="{{$product->photo->getUrl()}}" alt="Card image cap">

                    <div class="card-body">
                        <h4 class="card-title">{{$product->name}}</h4>
                        <p class="card-text">{{$product->description}}</p>
                        <p class="card-text">{{$product->price}}</p>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

@endsection





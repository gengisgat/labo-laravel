
<nav class="navbar navbar-expand navbar-light bg-primary"> {{-- emmet bs4-navbar-minimal-a --}}
    <div class="nav navbar-nav">
        <a class="nav-item nav-link {{ (empty(Request::segment(1))) ? 'active' : '' }}" href="/">Home </a>
        <a class="nav-item nav-link {{ (!empty(Request::segment(1)) && Request::segment(1) == 'products') ? 'active' : '' }}" href="{{url('/products')}}">Prodotti</a>
        <a class="nav-item nav-link {{ (request()->is('login')) ? 'active' : '' }}" href="{{url('/login')}}">Login</a>
    </div>
</nav>

<?php

Route::get('/', function () {
    // 1 prototipo
//   return '<h1>ciaomondo</h1>';

   // 2 prototipo Motore Eloquent (ORM)
//    $users = \App\User::all(); // select * from users
//    //dd($users); //dump and die, stampa a video la collezione
//
//    foreach ($users as $user) {
//        echo '<p>' . $user->firstname . ' ' . $user->lastname . '</p>';
//    }

    // 3 prototipo visualizzare i dati attraverso una vista
//    $users = \App\User::all(); // select * from users
//    //dd($users); //dump and die, stampa a video la collezione

//    return view('ciaomondo', compact('users')); // compact('users') = ['users' => $users]

    // 4 Homepage con le categorie di tutti i prodotti
    $categories = \App\ProductCategory::all();

    return view('guesthome', compact('categories'));
});

//////////////////////////////
// 5
//Route::get('/products/{categoryID?}', function ($categoryID = '%') {
////    dd($categoryID);
//    $category = \App\ProductCategory::where('id', $categoryID)->get();
//    $products = \App\Product::select('products.id', 'products.name', 'products.description', 'products.price', 'file_name', 'media.id as media_id')
//        ->join('product_product_category', 'product_product_category.product_id', '=', 'products.id')
//        ->join('media', 'media.model_id', '=', 'products.id')
//        ->where('media.model_type', 'App\\product')
//        ->where('product_product_category.product_category_id', 'like', $categoryID)
//        ->get();
////    dd($products);
//
//    return view('product.index', compact('category', 'products'));
//});


//////////////////////////////
// 6
Route::get('/products/{categoryID?}', 'GuestController@listproducts');

//Route::redirect('/', '/login');
Route::get('/home', function () {
    if (session('status')) {
        return redirect()->route('admin.home')->with('status', session('status'));
    }

    return redirect()->route('admin.home');
});

Auth::routes(['register' => false]);
// Admin

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['auth']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    // Permissions
    Route::delete('permissions/destroy', 'PermissionsController@massDestroy')->name('permissions.massDestroy');
    Route::resource('permissions', 'PermissionsController');

    // Roles
    Route::delete('roles/destroy', 'RolesController@massDestroy')->name('roles.massDestroy');
    Route::resource('roles', 'RolesController');

    // Users
    Route::delete('users/destroy', 'UsersController@massDestroy')->name('users.massDestroy');
    Route::resource('users', 'UsersController');

    // Product Categories
    Route::delete('product-categories/destroy', 'ProductCategoryController@massDestroy')->name('product-categories.massDestroy');
    Route::post('product-categories/media', 'ProductCategoryController@storeMedia')->name('product-categories.storeMedia');
    Route::post('product-categories/ckmedia', 'ProductCategoryController@storeCKEditorImages')->name('product-categories.storeCKEditorImages');
    Route::resource('product-categories', 'ProductCategoryController');

    // Product Tags
    Route::delete('product-tags/destroy', 'ProductTagController@massDestroy')->name('product-tags.massDestroy');
    Route::resource('product-tags', 'ProductTagController');

    // Products
    Route::delete('products/destroy', 'ProductController@massDestroy')->name('products.massDestroy');
    Route::post('products/media', 'ProductController@storeMedia')->name('products.storeMedia');
    Route::post('products/ckmedia', 'ProductController@storeCKEditorImages')->name('products.storeCKEditorImages');
    Route::resource('products', 'ProductController');

});
Route::group(['prefix' => 'profile', 'as' => 'profile.', 'namespace' => 'Auth', 'middleware' => ['auth']], function () {
// Change password
    if (file_exists(app_path('Http/Controllers/Auth/ChangePasswordController.php'))) {
        Route::get('password', 'ChangePasswordController@edit')->name('password.edit');
        Route::post('password', 'ChangePasswordController@update')->name('password.update');
    }

});

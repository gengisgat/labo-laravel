# Progetto il laboratorio Laravel SSSE.

Nel documento Configurazione PHPStorm.docx ci sono le istruzione per configurare il progetto con Laragon (solo per Windows)

## INSTALLARE E CONFIGURARE L'APPLICATIVO
Clona il repository
git clone git@gitlab.com:gengisgat/labo-laravel.git

cd labo-laravel 
Eventualmente spostare il contenuto di labo-laravel (anche la cartella nascosta .git) nella cartella preferita

Avvia il seguente comando per installare il framework e le librerie dell'applicativo:
```
composer install 
```

Avvia il seguente comando per creare il database e riempire le tabelle con dei dati iniziali:
```
php artisan migrate --seed 
```

Avvia il seguente comando per creare una chiave privata di criptazione (vedi .env):
```
php artisan key:generate 
```

Avvia il seguente comando per creare un link simbolico tra la cartella storage/app/public e la cartella public/storage:
```
php artisan storage:link 
```

## PER ACCEDERE ALL'APPLICATIVO

Utente amministrativo:
Username:	admin@admin.com
Password:	password

Gestore prodotti:
Username:	lino.topo@la.la
Password:	password

Utente semplice:
Username:	rino.pape@la.la
Password:	password